<!DOCTYPE HTML>
<html dir="ltr" lang="es">
<html>
	<head>
		<title>Prof. Dr. Helder Tedeschi</title>
		<link rel="shortcut icon" href="favicon.ico" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Aquí encontrará informaciones sobre su formación académica, las áreas en que actúa, los hospitales donde atiende, cómo contactarlo y la localización de sus  consultorios." />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.poptrox.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body id="top">
		<!-- Header -->
			<header id="header">
				<div class="banner">
					<a href="index.php" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
				</div>
				<div class="header_text">
				<h1 class="header-title"><strong>Prof. Dr. Helder Tedeschi</strong></h1>
				<p class="header-title">Neurocirugía<br>
				<a style="color: #a2a2a2 !important" href="http://lattes.cnpq.br/0103722914813250" target="_blank">CV</a></p>
				<sub>
							<p class="header-sub">Especialista en: <br>
							- Microcirugía oncológica cerebral y de la <br>médula espinal de niños y adultos.<br>
							- Meningiomas y Neurinomas del acústico.<br>
							- Aneurismas Cerebrales, Malas Formaciones <br>Arteriovenosos y Cavernomas.<br>
							- Cirugía de Epilepsia.</p>
				</sub>
				</div>
				<footer id="footer">
				<ul class="icons">
					<li><a href="https://www.linkedin.com/profile/view?id=85810223&trk=nav_responsive_tab_profile_pic" target="_blank" class="icon fa-linkedin-square"><span class="label">LinkedIn</span></a></li>
					<li><a href="#contato" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
				</ul>
				<ul class="copyright">
					<li>&copy; Todos los derechos reservados</li><li>Design: <a href="http://www.marcelotedeschi.com" target="_blank">Marcelo T</a></li><li><a href="index-en.php">English</a></li><li><a href="index.php">Português</a></li>
				</ul>
			</footer>
			</header>

		<!-- Main -->
			<div id="main">

				<!-- One -->
					<section id="one" class="panelSection">
						<header class="major">
							<h2>Bienvenido a la página del Profesor Doctor Helder Tedeschi</h2>
						</header>
						<p>Aquí encontrará informaciones sobre su formación académica, las áreas en que actúa, los hospitales donde atiende, cómo contactarlo y la localización de sus  consultorios.</p>
					</section>
				<!-- Formação Academica -->

					<section id="formacao" class="panelSection">
						<h2>Formación Académica</h2>
						<p>Médico especialista en neurocirugía, nacido el 26/01/1960, con formación y especialización en el extranjero en microcirugía vascular y oncológica cerebral, cirugía de la base del cráneo y anatomía 								microquirúrgica.  Conferencista invitado en innúmeros congresos, simposios y seminarios en el área de neurocirugía,  tanto en Brasil, como en el extranjero. Autor de varios trabajos en periódicos científicos 						nacionales e internacionales y editor del libro Doenças Cerebrovasculares – Diagnóstico e Tratamento  [Enfermedades Cerebrovasculares-Diagnóstico y Tratamiento] (Helder Tedeschi y Carlos Umberto Pereira, Eds., 						por la Revinter, 2004).  Habla  y escribe con fluidez en inglés y en español.<br><br>
						Doctor en Medicina por el Departamento de Neurología de la Facultad de Medicina de la Universidad de São Paulo (2000).<br><br>
						"Assistant Professor" de la Universidad de Flórida, Gainesville, Flórida, EEUU (1992).<br><br>
						"Research Fellow" em Microcirugía y Neuroanatomía del Departamento de Neurocirugía de la Universidad de Flórida, Gainesville, Flórida, EEUU, Servicio del Prof. Albert L. Rhoton Jr. (1989 - 1991).<br><br>
						"Visiting Fellow" del Departamento de Neurocirugía de la Universidad Hospital de Zurich, Zurich, Suiza, Servicio del Profesor M. Gazi Yasargil (1989).<br><br>
						"Clinical Fellow" del Departamento de Neurocirugía de la Universidad de Ljubljana, Ljubljana, Eslovenia , Servicio del Prof. Vinko V. Dolenc (1988/1989).<br><br>
						Residencia en Neurocirugía (5º año) en el Instituto Neurológico de São Paulo , SP, (1988).<br><br>
						Residencia médica en Neurología y Neurocirugía por la Facultad de Ciencias Médicas de la Santa Casa de Misericordia de São Paulo, SP, (1984 -1988).<br><br>
						Licenciado en Medicina por la Facultad de Ciencias Médicas de la Santa Casa de Misericordia de São Paulo, SP (1978 -1983).</p>
					</section>

				<!-- Areas de atuacao -->

					<section id="atuacao" class="panelSection">
						<h2>Áreas de actuación</h2>
						<p>Tiene experiencia en el área de Medicina, con énfasis en Neurocirugía y Microcirugía, actuando principalmente en los siguientes temas:</p>
						<ul>
							<li>Microcirugía oncológica cerebral y de la médula espinal de niños y adultos.</li>
							<li>Cirugía de la base del cráneo (meningiomas y neurinomas del acústico).</li>
							<li>Tratamiento quirúrgico de enfermedades cérebrovasculares – aneurismas cerebrales, malas formaciones arteriovenosos y cavernomas.</li>
							<li>Cirugía de epilepsia.</li>
						</ul>
					</section>

				<!-- experiencia profissional -->

					<section id="experiencia" class="panelSection">
						<h2>Experiencia Profesional</h2>
						<p>Actualmente forma parte del cuerpo docente del Departamento de Neurología y  es jefe de la asignatura de Neurocirugía de la Universidad Estadual de Campinas.<br><br> Es también el actual responsable del Equipo de Neurocirugía del Centro Oncologico Dr. Domingos A. Boldrini, en Campinas.</p>
						<h2>Experiencia en el extranjero</h2>
						<p>En Estados Unidos, es Profesor Asistente del Departamento de Neurocirugía de la Universidad de Flórida en Gainesville, Flórida.</p>
					</section>

				<!-- Sociedades -->

					<section id="sociedades" class="panelSection">
						<h2>Participación en Sociedades Profesionales</h2>
						<h3>En Brasil</h3>
						<ul>
							<li>Miembro Titular de la Sociedad Brasileña de Neurocirugía - SBN.</li>
							<li>Miembro Titular de la Academia Brasileña de Neurocirugía - ABNc.</li>
							<li>Miembro Titular Fundador de la Sociedad de Neurocirugía del Estado de São Paulo –SONESP.</li>
						</ul>
						<h3>En el exterior</h3>
						<ul>
							<li>Miembro de la American Association of Neurological Surgeons, E.U.A.</li>
							<li>Miembro del Congress of Neurological Surgeons, E.U.A.</li>
							<li>Miembro del North American Skull Base Society. E.U.A.</li>
							<li>Miembro del Asian Conference of Neurological Surgeons, en Asia.</li>
							<li>Miembro correspondiente de la Asociación Argentina de Neurocirugía, Argentina.</li>
							<li>Miembro de la Asociación Chilena de Cirugía de la Base del Cráneo, Chile.</li>
						</ul>
					</section>

				<!-- Publicacoes -->
 					<section id="publicacoes" class="panelSection">
 						<h2>Publicaciones</h2>
 						<p>Para acceder a todas sus publicaciones en la página del Google Scholar haga clic en este  <a href="http://scholar.google.com/citations?user=wK1KnX4AAAAJ&hl=sv&cstart=0&pagesize=20" target="_blank">enlace</a>.</p>
 					</section>

 				<!-- Hospitais de atuacao -->
					<section id="two" class="panelSection">
						<h2>Hospitales de Actuacion</h2>
						<div class="row">
							<article class="6u 12u$(3) work-item">
								<a href="images/Einstein.jpg" class="image fit thumb"><img src="images/Einstein.jpg" alt="" /></a>
								<h3>Hospital ALbert Einstein</h3>
							</article>
							<article class="6u 12u$(3) work-item">
								<a href="images/Samaritano.jpg" class="image fit thumb"><img src="images/Samaritano.jpg" alt="" /></a>
								<h3>Hospital Samaritano</h3>
							</article>
							<article class="6u$ 12u$(3) work-item">
								<a href="images/Hcor.jpg" class="image fit thumb"><img src="images/Hcor.jpg" alt="" /></a>
								<h3>Hospital do Coração</h3>
							</article>
							<article class="6u$ 12u$(3) work-item demais-hospitais">
								<h2>Otros</h2>
								<h3>En São Paulo</h3>
								<p>Hospital Alemão Oswaldo Cruz</p>
								<p>Hospital Santa Catarina</p>
								<p>Hospital Santa Paula</p>
								<p>Hospital São José</p>
								<p>Hospital Sírio e Libanês</p>
								<br>
								<h3>En Campinas</h3>
								<p>Hospital Vera Cruz</p>

							</article>
						</div>
					</section>

				<!-- contato -->
					<section id="contato" class="panelSection">
				<!-- Consultorios -->
						<h2>Contacto</h2>
						<div class="row">
							<div class="6u 12u$(3)">
								<h3>Consultorio en São Paulo</h3>
									<div class="7u$ 12u$(2)">
									<ul class="labeled-icons">
										<li>
											<h3 class="icon fa-home"><span class="label">Address</span></h3>
											Hospital Israelita Albert Einstein,<br />
											Av. Albert Einstein, 627<br />Bloco A1<br />
											CEP: 05652-900<br />
											São Paulo, SP<br />
											<a href="https://goo.gl/maps/oGiV4" target="_blank">Ver mapa</a>
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (11) 2151-9205
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (11) 3255-9396
										</li>
									</ul>
								</div>
							</div>
							<div class="6u$ 12u$(3)">
								<h3>Consultorio en Campinas</h3>
								<div class="7u$ 12u$(2)">
									<ul class="labeled-icons">
										<li>
											<h3 class="icon fa-home"><span class="label">Address</span></h3>
											Rua Antônio Lapa, 280<br />
											CEP: 13025-240<br />
											Campinas, SP<br />
											<a href="https://goo.gl/maps/MBVHe" target="_blank">Ver mapa</a>
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (19) 3395-8784
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (19) 3252-5442
										</li>
										<li>
											<h3 class="icon fa-mobile"><span class="label">Phone</span></h3>
											+55 (19) 99938-4238
										</li>
									</ul>
								</div>
							</div>
						</div>
					</section>

				<!-- Botoes de controle -->
				<div class="arrows">
					<a href="#" class="icon fa-arrow-circle-up prev-section"><span class="label">Próximo</span></a>
					<a href="#" class="icon fa-arrow-circle-down next-section"><span class="label">Anterior</span></a>
				</div>

			</div>
			<!-- Footer -->
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-63165325-1', 'auto');
			  ga('send', 'pageview');
			</script>
	</body>
</html>
