	<?php
		$lc = ""; // Initialize the language code variable
		// Check to see that the global language server variable isset()
		// If it is set, we cut the first two characters from that string
		if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
		    $lc = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		}
		// Now we simply evaluate that variable to detect specific languages
		if($lc == "es"){
		    header("Location: index-es.php");
		    exit();
		} else if($lc == "en"){
		    header("location: index-en.php");
		    exit();
		}
	?>
<!DOCTYPE HTML>
<html dir="ltr" lang="pt">
<html>
	<head>
		<title>Prof. Dr. Helder Tedeschi</title>
		<link rel="shortcut icon" href="favicon.ico" />
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="Página do Prof. Dr. Helder Tedeschi. Aqui, você encontrará importantes informações em relação a sua experiência profissional assim como informações de contato" />
		<meta name="keywords" content="neurocirurgia, epilepsia, Helder Tedeschi, laser, coluna, tumor cerebral, neurinoma, meningioma, aneurisma cerebral, neurologia, schwannoma, glioblastoma" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.poptrox.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-xlarge.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body id="top">
		<!-- Header -->
			<header id="header">
				<div class="banner">
					<a href="index.php" class="image avatar"><img src="images/avatar.jpg" alt="" /></a>
				</div>
				<div class="header_text">
				<h1 class="header-title"><strong>Prof. Dr. Helder Tedeschi</strong></h1>
				<p class="header-title">Neurocirurgia<br>
				<a class="nodecor-link" href="http://lattes.cnpq.br/0103722914813250" target="_blank">CV</a> &middot; <a href="#servicos" class="nodecor-link">Serviços</a></p>
				<sub>
							<p class="header-sub">Especialista em: <br>
							- Microcirurgia dos Tumores Cerebrais e da <br>
							Medula Espinal de Adultos e Crianças.<br>
							- Meningiomas e Neurinomas do Acústico.<br>
							- Aneurismas Cerebrais, Malformações<br> Arteriovenosas e Cavernomas.<br>
							- Cirurgia da Epilepsia.</p>
				</sub>
				</div>

				<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="https://www.linkedin.com/profile/view?id=85810223&trk=nav_responsive_tab_profile_pic" target="_blank" class="icon fa-linkedin-square"><span class="label">LinkedIn</span></a></li>
						<li><a href="#contato" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Todos os direitos reservados</li><li>Design: <a href="http://www.marcelotedeschi.com" target="_blank">Marcelo T</a></li><li><a href="index-en.php">English</a></li><li><a href="index-es.php">Español</a></li>
					</ul>
				</footer>
			</header>

		<!-- Main -->
			<div id="main">

				<!-- One -->
					<section id="one" class="panelSection">
						<header class="major">
							<h2>Bem-vindo à página do Prof. Dr. Helder Tedeschi</h2>
						</header>
						<p>Aqui, você encontrará informações sobre sua formação acadêmica, suas áreas de atuação,
						os hospitais onde atende, seu contato e localização de seus consultórios.</p>
					</section>
				<!-- Formação Academica -->

					<section id="formacao" class="panelSection">
						<h2>Formação Acadêmica</h2>
						<p>Médico especialista em neurocirurgia, nascido em 26/01/1960, com formação e especialização no exterior em microcirurgia vascular e de tumores cerebrais, cirurgia da base craniana e
						 anatomia microcirúrgica. Palestrante convidado em inúmeros congressos, simpósios e seminários na área de neurocirurgia, no Brasil e no exterior. Autor de vários trabalhos
						  em periódicos científicos nacionais e internacionais e editor do livro Doenças Cerebrovasculares – Diagnóstico e Tratamento (Helder Tedeschi e Carlos Umberto Pereira, Eds.,
						   pela Revinter, 2004). Fluente em inglês e espanhol (conversação e escrita). <br><br>
						Doutor em Medicina pelo Departamento de Neurologia da Faculdade de Medicina da Universidade de São Paulo (2000).<br><br>
						"Assistant Professor" da Universidade da Flórida, Gainesville, Flórida, EUA. (1992). <br><br>
						"Research Fellow" em Microcirurgia e Neuroanatomia do Departamento de Neurocirurgia da Universidade da Flórida, Gainesville, Flórida, EUA, Serviço do Prof. Albert L.
						Rhoton Jr. (1989 - 1991).<br><br>
						"Visiting Fellow" do Departamento de Neurocirurgia da Universidade Hospital de Zurich, Zurich, Suíça, Serviço do Prof. M. Gazi Yasargil (1989).<br><br>
						"Clinical Fellow" do Departamento de Neurocirurgia da Universidade de Ljubljana, Ljubljana, Eslovenia , Serviço do Prof. Vinko V. Dolenc (1988/1989).<br><br>
						Residência em Neurocirurgia (5º ano) no Instituto Neurológico de São Paulo , SP, (1988).<br><br>
						Residência médica em Neurologia e Neurocirurgia pela Faculdade de Ciências Médicas da Santa Casa de Misericórdia de São Paulo, SP, (1984 -1988).<br><br>
						Faculdade de Medicina pela Faculdade de Ciências Médicas da Santa Casa de Misericórdia de São Paulo, SP (1978 -1983).</p>
					</section>

				<!-- Areas de atuacao -->

					<section id="atuacao" class="panelSection">
						<h2>Áreas De Atuação</h2>
						<p>Tem experiência na área de Medicina, com ênfase em Neurocirurgia e Microcirurgia, atuando principalmente nas seguintes sub-áreas:</p>
						<ul>
							<li>Microcirurgia dos Tumores Cerebrais e da Medula Espinal de adultos e crianças.</li>
							<li>Cirurgia da Base do Crânio (Meningiomas e Neurinomas do Acústico).</li>
							<li>Tratamento Cirúrgico de Doenças Cerebrovasculares - Aneurismas Cerebrais, Malformações Arteriovenosas e Cavernomas.</li>
							<li>Cirurgia da Epilepsia.</li>
						</ul>
					</section>

				<!-- experiencia profissional -->

					<section id="experiencia" class="panelSection">
						<h2>Experiência Profissional</h2>
						<p>Atualmente é Professor Docente do Departamento de Neurologia e Chefe da Disciplina de Neurocirurgia da Universidade Estadual de Campinas.<br><br> É também o atual responsável pela Equipe de Neurocirurgia do Centro Oncológico Dr. Domingos A. Boldrini, em Campinas.</p>
						<h2>Experiência no exterior</h2>
						<p>Nos Estados Unidos, é Professor Assistente do Departamento de Neurocirurgia da Universidade da Flórida em Gainesville, Flórida.</p>
					</section>

				<!-- Sociedades -->

					<section id="sociedades" class="panelSection">
						<h2>Participação Em Sociedades Profissionais</h2>
						<h3>No Brasil</h3>
						<ul>
							<li>Membro Titular da Sociedade Brasileira de Neurocirurgia – SBN.</li>
							<li>Membro Titular da Academia Brasileira de Neurocirurgia - ABNc.</li>
							<li>Membro Titular Fundador da Sociedade de Neurocirurgia do Estado de São Paulo – SONESP.</li>
						</ul>
						<h3>No Exterior</h3>
						<ul>
							<li>Membro da American Association of Neurological Surgeons, E.U.A.</li>
							<li>Membro do Congress of Neurological Surgeons, E.U.A.</li>
							<li>Membro da North American Skull Base Society. E.U.A.</li>
							<li>Membro do Asian Conference of Neurological Surgeons, Ásia.</li>
							<li>Membro Correspondente da Asociación Argentina de Neurocirugía, Argentina.</li>
							<li>Membro da Asociación Chilena de Cirugía da Basis Del Cráneo, Chile.</li>
						</ul>
					</section>

				<!-- Publicacoes -->
 					<section id="publicacoes" class="panelSection">
 						<h2>Publicações</h2>
 						<p>É possível ver todas as publicações em sua página do Google Scholar, neste <a href="http://scholar.google.com/citations?user=wK1KnX4AAAAJ&hl=sv&cstart=0&pagesize=20" target="_blank">link</a>.</p>
 					</section>

 				<!-- Servicos -->
 					<section id="servicos" class="panelSection">
	 					<h2>Serviços</h2>
	 					<!--
<h4>Segunda Opinião:</h4>
	 						<p>Gostaria de entender melhor sua condição médica? Tirar possíveis dúvidas existentes sobre seu diagnóstico ou talvez encontrar melhores opções de tratamento? Envie um email por este <a href="#contatoEmail">link</a> e agende uma consulta eletrônica para ter uma segunda opinião.</p>
-->

	 					<div id="servicos-pop">
	 						<h4>Microcirurgia Intracraniana:</h4>
	 						<ul>
		 						<li><a href="servicos.php?input=tumores_cerebrais" data-poptrox="ajax">Tumores Cerebrais</a></li>
		 						<li><a href="servicos.php?input=paciente_acordado" data-poptrox="ajax">Cirurgia com o paciente "acordado"</a></li>
		 						<li><a href="servicos.php?input=adenomas_pituitarios" data-poptrox="ajax">Adenomas Pituitários</a></li>
		 						<li><a href="servicos.php?input=schwanomas" data-poptrox="ajax">Schwanomas Vestibulares(Neurinoma do Acústico)</a></li>
		 						<li><a href="servicos.php?input=epilepsia" data-poptrox="ajax">Cirurgia da Epilepsia</a></li>
		 						<li><a href="servicos.php?input=vascular" data-poptrox="ajax">Vascular(Aneurismas, Malformações, Cavernomas)</a></li>
	 						</ul>
	 						<h4>Microcirurgia Espinal (Coluna Vertebral e Medula Espinal):</h4>
	 						<ul>
		 						<li><a href="servicos.php?input=tumores_medula" data-poptrox="ajax">Tumores da medula</a></li>
		 						<li><a href="servicos.php?input=microdiscectomia-cervical" data-poptrox="ajax">Microdiscectomia Cervical Anterior</a></li>
		 						<li><a href="servicos.php?input=microdiscectomia-lombar" data-poptrox="ajax">Microdiscectomia Lombar</a></li>
	 						</ul>
	 						<h4>Outros:</h4>
	 						<ul>
		 						<li><a href="servicos.php?input=neuronavegacao" data-poptrox="ajax">Neuronavegação Estereotáxica Computadorizada</a></li>
		 						<li><a href="servicos.php?input=radiocirurgia" data-poptrox="ajax">Radiocirurgia Estereotáxica</a></li>
		 						<li><a href="servicos.php?input=laser" data-poptrox="ajax">Laser e Neurocirurgia</a></li>
	 						</ul>

	 					</div>
 					</section>

				<!-- Hospitais de atuacao -->
					<section id="two" class="panelSection">
						<h2>Hospitais de Atuação</h2>
						<div class="row">
							<article class="6u 12u$(3) work-item">
								<a href="images/Einstein.jpg" class="image fit thumb"><img src="images/Einstein.jpg" alt="" /></a>
								<h3>Hospital Albert Einstein</h3>
							</article>
							<article class="6u 12u$(3) work-item">
								<a href="images/Samaritano.jpg" class="image fit thumb"><img src="images/Samaritano.jpg" alt="" /></a>
								<h3>Hospital Samaritano</h3>
							</article>
							<article class="6u$ 12u$(3) work-item">
								<a href="images/Hcor.jpg" class="image fit thumb"><img src="images/Hcor.jpg" alt="" /></a>
								<h3>Hospital do Coração</h3>
							</article>
							<article class="6u$ 12u$(3) work-item demais-hospitais">
								<h2>Demais hospitais</h2>
								<h3>Em São Paulo</h3>
								<p>Hospital Alemão Oswaldo Cruz</p>
								<p>Hospital Santa Catarina</p>
								<p>Hospital Santa Paula</p>
								<p>Hospital São José</p>
								<p>Hospital Sírio e Libanês</p>
								<br>
								<h3>Em Campinas</h3>
								<p>Hospital Vera Cruz</p>
							</article>
						</div>
					</section>


				<!-- contato -->
					<section id="contato" class="panelSection">
				<!-- Consultorios -->
						<h2>Contato</h2>
						<div class="row">
							<div class="6u 12u$(3)">
								<h3>Consultório Em São Paulo</h3>
									<div class="7u$ 12u$(2)">
									<ul class="labeled-icons">
										<li>
											<h3 class="icon fa-home"><span class="label">Address</span></h3>
											Hospital Israelita Albert Einstein,<br />
											Av. Albert Einstein, 627<br />Bloco A1<br />
											CEP: 05652-900<br />
											São Paulo, SP<br />
											<a href="https://goo.gl/maps/oGiV4" target="_blank">Ver mapa</a>
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (11) 2151-9205
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (11) 3255-9396
										</li>
									</ul>
								</div>
							</div>
							<div class="6u$ 12u$(3)">
								<h3>Consultório Em Campinas</h3>
								<div class="7u$ 12u$(2)">
									<ul class="labeled-icons">
										<li>
											<h3 class="icon fa-home"><span class="label">Address</span></h3>
											Rua Antônio Lapa, 280<br />
											CEP: 13025-240<br />
											Campinas, SP<br />
											<a href="https://goo.gl/maps/MBVHe" target="_blank">Ver mapa</a>
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (19) 3395-8784
										</li>
										<li>
											<h3 class="icon fa-phone"><span class="label">Phone</span></h3>
											+55 (19) 3252-5442
										</li>
										<li>
											<h3 class="icon fa-mobile"><span class="label">Phone</span></h3>
											+55 (19) 99938-4238
										</li>
									</ul>
								</div>
							</div>
						</div>
					</section>

			<!-- Botoes de controle -->
				<div class="arrows">
					<a href="#" class="icon fa-arrow-circle-up prev-section"><span class="label">Próximo</span></a>
					<a href="#" class="icon fa-arrow-circle-down next-section"><span class="label">Anterior</span></a>
				</div>

			</div>

			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-63165325-1', 'auto');
			  ga('send', 'pageview');
			</script>
	</body>
</html>
