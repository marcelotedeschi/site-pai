<html>
	<head>
	</head>
	<body>

	<?php

	// The value of the variable name is found
	$input = $_GET['input'];
	
	if ($input == "tumores_cerebrais") { ?>
		<h1>Gliomas e Astrocitomas</h1>
		<p>Tumores cerebrais primários( tumores secundários são as chamadas metástases, que possuem sua origem em tumores de outra localidade do organismo e que chegam ao cérebro através de células levadas pela corrente sangüínea), chamados gliomas ou astrocitomas são tumores das células estruturais do cérebro, conhecidas como células gliais. Esses tumores podem se apresentar com fraqueza, cefaléia ou crises convulsivas, ou alterações de personalidade. Alguns astrocitomas são considerados de baixo grau  (low-grade) e podem ser benignos e de crescimento lento. Astrocitomas mais agressivos, chamados glioblastoma multiforme, são geralmente rapidamente progressivos e malígnos. 
Quando a cirurgia é indicada, o objetivo é promover uma ressecção segura e, se possível total, além de estabelecer o grau histológico. O uso de navegação computadorizada estereotáxica como guia durante a cirurgia tem melhorado sobremaneira a extensão de ressecção e permitido cirurgias menos invasivas. Em casos selecionados biósias guiadas por estereotaxia podem ser indicadas.
O tratamento é coordenado pelo neurocirurgião junto a um time multidisciplinar que envolve neuro-oncologistas e oncologistas radioterapêutas. Quando apropriado, os pacientes podem ser candidatos a protocolos de quimioterapia após a cirurgia. 
Nosso objetivo é promover um programa de tratamento compreensivo e individualizado para cada paciente.</p>
	<h1>Meningeomas</h1>
	<p>Meningeomas são tumores de crescimento lento, geralmente benignos, que se originam das membranas que envolvem o cérebro. À medida que crescem, os meningeomas podem comprimir o tecido cerebral circunjacente ou os nervos e causar sintomas neurológicos . Freqüentemente os meningeomas causam inchaço do cérebro ocasionando assim os sintomas. Mesmo pequenos meningeomas podem causar fraqueza, cefaléia ou crises convulsivas. Embora a vasta maioria dos meningeomas seja de natureza benígna, estes podem atuar de maneira agressiva devido à sua natureza ou localização. 
Na avaliação das opções de tratamento, levamos em consideração o tamanho, a localização do tumor e a idade do paciente. Por exemplo, podemos recomendar observação e acompanhamento com RNM em pacientes selecionados com pequenos tumores. Nos pacientes submetidos à cirurgia para ressecção do tumor, o objetivo é o de remover de maneira segura o tumor e aliviar a pressão exercida sobre o cérebro. Freqüentemente somos capazes de remover totalmente o tumor sem riscos para o paciente. Utilizamos de equipamentos sofisticados como navegação computadorizada estereotáxica para aumentar a precisão de nossas ressecções e minimizar eventuais danos neurológicos. Alguns pacientes com meningeomas podem ser candidatos a radiocirurgia estereotáxica.
</p>
<?php }

	if ($input == "paciente_acordado") { ?>
		<h1>Cirurgia com o paciente “acordado”</h1>
		<p>Em casos nos quais tumores ou outras lesões cerebrais localizam-se em regiões chamadas eloquentes(responsáveis pela fala, motricidade ou visão) o paciente pode ser operado com técnica anestésica especial que, embora evite a dor, não altera a consciência do indivíduo(cirurgia com o paciente desperto ou acordado) e permite a remoção da lesão sem ocasionar sequelas ao paciente.</p>
<?php }

	if ($input == "adenomas_pituitarios") { ?>
		<h1>Adenomas Pituitários</h1>
		<p>Adenomas pituitários são tumores benígnos localizados na hipófise, a glândula pituitária. A glândula pituitária controla vários hormônios no corpo. Ela é localizada logo abaixo dos nervos ópticos que veiculam a visão. Assim pacientes com adenomas pituitários podem apresentar distúrbios hormonais ou problemas visuais se o tumor tiver crescido o suficiente para causar compressão dos nervos ópticos. O exame de ressonância irá demonstrar a localizaçãoe o tamanho do adenoma e o grau de compressão dos nervos ópticos causado pelo tumor. Testes hormonais e dos campos visuais são também realizados na avaliação de pacientes com adenomas.
Tumores pituitários são divididos em hormonalmente ativos, tumores que secretam ativamente hormônios pituitários e tumores não funcionantes, que não demonstram atividade secretora hormonal. Aproximadamente20% dos adenomas irá secretar níveis altos do hormônio prolactina. Prolactinomas são tratáveis com medicação e cirurgia é geralmente reservada para casos especiais. Entretanto, a grande maioria dos pacientes com prolactinomas sintomáticos irá necessitar tratamento cirúrgico. 
Para pacientes com adenomas pituitários que requeiram cirurgia realizamos uma microcirurgia trans-esfenoidal que acessa o tumor diretamente através de uma das narinas sem deixar cicatriz aparente. Essa cirurgia é realizada conjuntamente com um cirurgião otorrinolaringologista que realiza o acesso através dos seios nasais com o auxílio de endoscopia e o tumor é removido com auxílio do microscópio cirúrgico. A estada no hospital após a cirurgia trans-esfenoidal é de 2 a 3 dias. Os pacientes retornam às suas atividades plenas em 2 a 3 semanas. 
</p>
<?php }

	if ($input == "schwanomas") { ?>
		<h1>Neurinoma do Acústico</h1>
		<p>O neurinoma do acústico( Schwanoma vestibular ou neurilemoma) é um tumor benígno com origem no oitavo nervo craniano contido no ouvido interno. Esse nervo possui duas partes distintas, uma associada à transmissão do som e outra que envia informações sobre o equilíbrio do corpo do ouvido interno para o cérebro. Esse nervo juntamente com o nervo facial( VII nervo craniano que é responsável pela motilidade dos músculos da face) passa por um canal ósseo chamado conduto auditivo interno.
Os neurinomas do acústico crescem lentamente ao longo de anos. Embora não invada o tecido cerebral este é deslocado à medida que o tumor cresce. O tumor ao crescer geralmente protrai do conduto auditivo interno para a área atrás do osso temporal conhecida com ângulo ponto-cerebelar. Tumores maiores podem acometer outros nervos cranianos ou mesmo comprimir o tronco cerebral, estrutura importante na manutenção de funções vitais e trazer risco à vida se não tratado.

Embora possam ocorrer de maneira hereditária em pacientes portadores de uma doença chamada neurofibromatose(NF2), a maioria dos tumores ocorre de maneira espontânea e sem causa aparente. O sintoma mais comum de apresentação é a perda de audição no lado do tumor, zumbidos e tonturas também podem ocorrer. 
O tratamento depende da idade e condição clínica do paciente e pode ser conservador, remoção cirúrgica com auxílio de monitorização eletrofisiológica ( principalmente indicada nos casos em que se pretende salvar a audição) e radiocirurgia. A indicação de tratamento deve ser avaliada junto ao neurocirurgião e é individual.</p>
<?php }

	if ($input == "epilepsia") { ?>
		<h1>Cirurgia da Epilepsia</h1>
		<p>A cirurgia pode ser empregada como tratamento em aproximadamente 10 a 20% dos indivíduos com epilepsia refratária à medicação. Pacientes com crises parciais ou mesmo crises complexas generalizadas são candidatos para cirurgia. Em alguns casos pacientes requerem cirurgia para remoção de lesões ou mesmo de áreas cerebrais que estejam causando epilepsia.  A remoção cirúrgica é efetuada após cuidadosa investigação neurológica e neurofisiológica realizada em centro especializado.</p>
<?php }

	if ($input == "vascular") { ?>
		<h1>Aneurismas Cerebrais</h1>
		<p>Um aneurisma cerebral é a dilatação ou abaulamento da parede de uma artéria e, menos comumente, de uma veia no cérebro. A lesão pode ser o resultado de defeitos congênitos ou de outras condições como pressão alta, aterosclerose (a formação de depósitos gordurosos nas artérias), ou trauma de crânio. Aneurismas cerebrais podem acontecer em qualquer idade, embora eles sejam mais comuns em adultos que em crianças e ligeiramente mais comuns em mulheres que em homens. Os sinais e sintomas de um aneurisma cerebral que não rompeu dependerão em parte de seu tamanho e taxa de crescimento. Por exemplo, um pequeno aneurisma que não se modifica, geralmente não produzirá nenhum sintoma, ao passo que um aneurisma maior que cresce continuamente  pode produzir sintomas como perda de sensibilidade na face ou problemas com os olhos. Imediatamente antes de um aneurisma romper, o indivíduo pode experimentar sintomas tais como dor de cabeça súbita e geralmente importante, náuseas, prejuízo da visão, vômitos, e perda de consciência. 
Em geral a ruptura de um aneurisma cerebral resulta em sangramento no cérebro,  causando um derrame. O sangue pode também escoar para os arredores do cérebro e desenvolver  um hematoma  intracraniano (um coágulo de sangue dentro do crânio). Ressangramento, hidrocefalia (o acúmulo excessivo do líquido cérebro-espinal), vasospasmo (espasmo dos vasos sanguíneos), ou aneurismas adicionais também podem acontecer.
Tratamento de emergência para indivíduos com um aneurisma cerebral roto geralmente inclui medidas clínicas que visam reduzir a pressão intracraniana. 
A cirurgia normalmente é executada dentro dos primeiros 3 dias para "clipar"  o aneurisma roto( oclusão da base do aneurisma com pequeno clipe metálico inofensivo ao organismo) e reduzir o risco de ressangramento. Em pacientes para os quais a cirurgia é considerada muito arriscada podem ser realizadas técnicas alternativas como a embolização e trombose do aneurisma. Durante estes procedimentos, um fino tubo oco (cateter) é inserido por uma artéria para viajar até o cérebro. Uma vez que o cateter alcança o aneurisma, são usadas "micromolas"(coils) ou cola para bloquear o fluxo de sangue pelo aneurisma. Outros tratamentos podem incluir repouso, terapia medicamentosa ou em casos de vasospasmo, terapia  hipertensiva-hipervolêmica (que eleva a pressão sanguínea, aumenta o volume de de líquidos nas artérias e afina o sangue) para dirigir o fluxo de sangue através e ao redor das artérias  bloqueadas pelo vasospasmo.
O prognóstico para um paciente com um aneurisma cerebral roto depende da extensão e local do aneurisma, da idade da pessoa, saúde geral, e condição neurológica. Alguns indivíduos com aneurisma cerebral roto morrem da hemorragia inicial. Outros indivíduos recuperam-se com pouco ou com nenhum déficit neurológico. Diagnóstico precoce e tratamento são importantes.

	<h1>Malformações Arteriovenosas Cerebrais</h1>
	<p>Uma malformação arteriovenosa (MAV) é uma desordem congênita caracterizada por um complexo emaranhado de artérias e veias. Uma MAV pode acontecer no cérebro, cerebelo,tronco cerebral ou medula espinhal e é causada pelo desenvolvimento anormal de vasos sanguíneos. Os sintomas mais comuns de MAV incluem hemorragia (sangramento), ataques epilépticos, dores de cabeça e problemas neurológicos como paralisia ou perda de fala, memória, ou visão.
Há três formas gerais de tratamento para MAV: cirurgia; embolização que envolve o fechamento dos vasos do MAV injetando-se cola neles (embolização é freqüentemente usada antes da cirurgia); e radiocirurgia que envolve radiação focalizada  na MAV.
MAVs que sangram podem levar a problemas neurológicos sérios, e às vezes morte. Porém, algumas pessoas têm MAVs que nunca causam problemas.
Ataques epilépticos e dores de cabeça são os sintomas  mais generalizados de MAVs, mas nenhum tipo particular de ataque epiléptico ou padrão de dor de cabeça foi identificado. 
MAVs também podem causar uma gama extensa de sintomas neurológicos mais específicos que variam de pessoa a pessoa, dependendo principalmentedo local do MAV. Tais sintomas podem incluir fraqueza muscular ou paralisia em uma parte do corpo; perda de coordenação (ataxia) que pode conduzir a  problemas tais como perturbações da marcha; apraxia, ou dificuldades em levar a cabo tarefas que requeiram planejamento; vertigem; perturbações visuais como  perda de uma parte do campo visual; uma inabilidade para controlar movimentos dos olhos; papiledema (inchaço de uma parte do nervo óptico conhecido como o disco óptico); vários problemas em usar ou compreender o idioma  (afasia); sensações anormais como adormecimento, formigamento, ou dor espontânea (parestesia ou disestesia); déficits de memória; e confusão mental, alucinações, ou demência. Os investigadores descobriram evidência recentemente que MAVs também podem causar desordens sutis de aprendizagem  ou  comportamento em algumas pessoas durante sua infância ou adolescência, muito antes que sintomas mais óbvios ficassem evidentes.</p>

</p>
<?php }

	if ($input == "tumores_medula") { ?>
		<h1>Tumores Espinais</h1>
		<p>Existem vários tipos diferentes de tumores que podem afetar a coluna espinal e a medula. Os tumores espinais podem causar simtomas por pressionarem os nervos ou a medula espinal. Os pacientes podem apresentar dores nas costas, fraqueza, dificuldade para andar ou falta de equilíbrio. O exame de ressonância nuclear magnética é o exame principal no diagnóstico desses tumores.
</p>
		<h1>Meningeomas Espinais</h1>
		<p>Meningeomas são tumores benígnos das membranas que envolvem a medula espinal. Esses tumores de crescimento lento geralmente afetam mulheres e usualmente se localizam na coluna torácica. Meningeomas espinais podem causar dor pela compressão de raízes nervosas  ou causar fraqueza e perda de sensibilidade nas pernas se ocorrer compressão da medula espinal. Nos pacientes com compressão da medula espinal, a cirurgia é geralmente recomendada. Embora existam riscos em qualquer cirurgia, na grande maioria dos casos o tumor pode ser removido sem agregarem-se novos déficits neurológicos. O uso de técnicas microcirúrgicas e de aparelhagem de última geração contribuem para excelentes resultados com baixa morbidade e curto período de internação.</p>
		<h1>Schwannomas/Neurofibromas</h1>
		<p>Schwannomas ou neurofibromas são tumores benígnos dos nervos espinais. Esses tumores podem ocorrer em qualquer local ao longo da coluna vertebral assim como nos nervos periféricos. Eles tipicamente causam dor ao longo da raíz nervosa semelhante a um “nervo pinçado”.  Pacientes com tumores sintomáticos ou com compressão medular são submetidos a cirurgia com excelentes resultados dependendo da localização e tamanho do tumor  e do tempo de instalação de déficit neurológico. </p>
		<h1>Tumores Espinais Metastáticos</h1>
		<p>Tumores metastáticos espinais são achados relativamente comuns. Os pacientes podem se apresentar com dor ou deficits neurológicos. Geralmente são diagnosticados com RNM. Dependendo da extensão do envolvimento do tumor, compressão da medula espinal ou grau de déficit neurológico, diferentes opções de tratamento podem ser consideradas. A cirurgia pode ser necessária nos pacientes com instabilidade da coluna ou com compressão medular significativa. Radioterapia é também usada em vários pacientes com tumores metastáticos.</p>
		<h1>Tumores Espinais Intramedulares</h1>
		<p>Tumores que se originam da substância da medula espinal são chamados intramedulares. Os tipos mais comuns são astrocitoma, ependimoma, e ganglioglioma. A grande maioria deles é benígna e de crescimento lento. Devido ao seu crescimento lento, esses tumores podem permanecer assintomáticos e não serem detectados por um tempo considerável. Sintomas são notados com o passar de meses ou anos. Uma apresentação comum em crianças é dor nas costas, caracteristicamente pior à noite, de difícil controle com medicação analgésica. Pode levar muito tempo para que sintomas mais graves como diminuição de sensibilidade ou fraqueza muscular apareçam. Agulhadas ou sensação de queimação no corpo, chamadas disestesias, abaixo do nível do tumor são sintomas freqüentes.

Hoje em dia os tumores são diagnosticados após exame com RNM. A primeira opção de tratamento em praticamente todos os casos é a cirurgia, já que uma parcela significativa de pacientes com tumores intramendulares benignos pode ser curada com cirurgia somente.
</p>
<?php }

	if ($input == "microdiscectomia-cervical") { ?>
		<h1>Microdiscectomia Cervical Anterior</h1>
		<p>A herniação do disco cervical é um problema de coluna bastante comum. O disco cervical herniado pode comprimir os nervos que se direcionam para os braços ou comprimir a medula espinal cervical. Os sintomas podem incluir dor cervical, nos braços ou mesmo fraqueza muscular. A compressão da medula espinal pode resultar em danos mais sérios tais como fraqueza nas pernas ou dificuldade para caminhar. 
Na maioria dos casos o tratamento é apenas conservador. O tempo, os antiinflamatórios e a fisioterapia são geralmente utilizados com sucesso.
 Cirurgia para discos herniados é considerada nos pacientes que continuam com sintomas apesar das medidas conservadoras. Sintomas neurológicos, tais como fraqueza ou dificuldade à marcha, podem fazer com que a indicação cirúrgica seja mais precoce. 
A cirurgia mais comum para discos cervicais herniados sintomáticos é a discectomia cervical anterior microscópica com fusão, que pode ser feita em um único ou em múltiplos níveis conforme necessário. A cirurgia de discectomia cervical anterior é realizada via uma pequena incisão na frente do pescoço. Técnicas microcirúrgicas são utilizadas para cuidadosamnete remover o disco herniado e esporões ósseos aliviando assim a pressão sobre os nervos ou a medula espinal. Após a cirurgia os pacientes geralmente utilizal um colar cervical macio para conforto por algumas semanas. Em geral os pacientes retornam ao trabalho em duas semanas. O tempo cirúrgico é bastante curto e os pacientes geralmente recebem alta em um a dois dias.</p>
<?php }

	if ($input == "microdiscectomia-lombar") { ?>
		<h1>Microdiscectomia Lombar</h1>
		<p>Discos lombares herniados podem causar uma variedade de sintomas que incluem dor nas costas, ciática e fraqueza nas pernas. Os sintomas são causados pelo disco herniado pressionando uma raiz nervosa. Na maioria dos casos o tratamento inicial é apenas conservador e inclui anti-inflamatórios, relaxantes musculares e fisioterapia. Em alguns casos selecionados a injeção epidural de esteróides pode ser aconselhada. Apenas uma pequena porcentagem de pacientes irá requerer cirurgia. 
A microdiscectomia lombar é reservada a pacientes nos quais o tratamento conservador falhou ou com dor contínua. Pacientes com déficits motores( fraqueza muscular ou incapacidade funcional) podem ser candidatos a cirurgia mais precocemente. O objetivo da cirurgia é remover o disco herniado e aliviar a pressão sobre a raíz nervosa. Uma pequena incisão é realizada na região lombar e com o auxílio de raio-X e do microscópio cirúrgico o disco é removido e a raiz liberada. Os pacientes podem já caminhar ao fim de algumas horas após o procedimento e geralmente experimentam remissão completa ou quase completa dos sintomas imediatamente. A alta é obtida após um par de dias e o retorno ao trabalho se dá ao cabo de algumas semanas. </p>
<?php }

	if ($input == "neuronavegacao") { ?>
		<h1>Neuronavegação Estereotáxica Computadorizada</h1>
		<p>Um dos maiores avanços em Neurocirurgia na última década tem sido a introdução de computadores na sala de operações nas cirurgias de crânio e coluna. Durante nossas cirurgias freqüentemente utilizamos um sistema de navegação estereotáxica computadorizada. Para cirurgia dos tumores cerebrais essas técnicas estereotáxicas nos permitem planejar nossa abordagem ao tumor e durante a cirurgia localizar nossa posição exata no cérebro. Essa tecnologia “state-of-the-art” nos possibilita uma cirurgia mais segura ao auxiliar o cirurgião a evitar áreas críticas do cérebro. Além disso, a habilidade de “navegar” dentro do tumor permite ressecções tumorais mais completas. Antes da cirurgia os pacientes realizam uma tomografia ou ressonância magnética para o planejamento cujas informações são transferidas ao computador e processadas. Na cirurgia o cirurgião trabalha com um monitor de vídeo que continuamente o guia durante o ato cirúrgico. 
</p>
<?php }

	if ($input == "radiocirurgia") { ?>
		<h1>Radiocirurgia Estereotáxica</h1>
		<p>Radiocirurgia estereotáxica é uma técnica para o tratamento das desordens neurológicas cerebrais com o uso de uma única sessão de radiação localizada. Isso é geralmente realizado em nível ambulatorial ao longo de um único dia. A maioria dos pacientes tratados com radiocirurgia estereotáxica possuem tumores cerebrais, benignos ou malignos. A radiocirurgia aplica uma alta dose de radiação a uma área alvo enquanto evita áreas de tecido normal ao redor da lesão. A radiocirurgia pode ser usada como complemento ao tratamento cirúrgico ou mesmo como seu substituto. A recomendação do tratamento depende do tipo do tumor, sua localização e da idade e condição clínica do paciente. A radiocirurgia pode também ser utilizada como adjuvante ou msmo como tratamento “per se” de algumas malformações artério-venosas cerebrais.
Utilizando-se de uma estação de planejamento computadorizado 3-D, um plano de tratamento é constituído a fim de se aplicar uma dose máxima de radiação à lesão com mínima exposição de estruturas críticas circunjacentes. O tratamento é realizado com um Acelerador Linear e é indolor. 
</p>
<?php }

	if ($input == "laser") { ?>
		<h1>Laser e neurocirurgia</h1>
		<p>Embora bastante divulgada, a utilização de lasers em neurocirurgia passou por fases distintas de grande entusiasmo e de total decepção. Atualmente a utilização de lasers está restrita a grandes centros dos EUA em caráter experimental para o tratamento de casos selecionados de tumores da medula.
Nosso centro aguarda liberação da ANVISA para importação do equipamento(atualmente indisponível no Brasil).
</p>
<?php }



	?>

	</body>
	</html>